package com.paic.arch.interviews;

import org.apache.commons.lang.StringUtils;

/**
 * @author linyuanxing
 * @version 1.0
 * @since 2018/3/4
 */
public class MyTimeConverter implements TimeConverter {

    private static final String NEWLINE = System.getProperty("line.separator");
    private static final String YELLOW = "Y";
    private static final String RED = "R";
    private static final String OFF = "O";

    @Override
    public String convertTime(String aTime) {
        if (StringUtils.isNotBlank(aTime)) {
            String[] times = aTime.split(":");
            if (times.length == 3) {
                // aTime的格式为HH:mm:ss, 转化成时分秒的数字
                int h = Integer.parseInt(times[0]);
                int m = Integer.parseInt(times[1]);
                int s = Integer.parseInt(times[2]);

                StringBuilder sb = new StringBuilder();
                sb.append(convertSecond(s)).append(NEWLINE)
                        .append(convertHour(h)).append(NEWLINE)
                        .append(convertMinute(m));
                return sb.toString();
            }
        }
        return null;
    }

    private String convertHour(int h) {
        StringBuilder hour = new StringBuilder();
        // 输出第二排的亮灯情况
        // 第二排一个灯代表5个小时，除于5，几个5的倍数亮几个红灯，其余不亮
        int a = h / 5;
        for (int i = 0; i < 4; i++) {
            hour.append(a > i ? RED : OFF);
        }
        hour.append(NEWLINE);

        // 输出第三排的亮灯情况
        // 第三排一个灯代表1个小时，对5求余，余数是多少就亮多少红灯，其余不亮
        int b = h % 5;
        for (int i = 0; i < 4; i++) {
            hour.append(b > i ? RED : OFF);
        }

        return hour.toString();
    }

    private String convertMinute(int m) {
        StringBuilder minute = new StringBuilder();

        // 输出第四排的亮灯情况
        // 分钟除于5就是亮灯的数量，剩余的不亮，其中第3、第6和第9盏的灯是红灯，其余亮黄灯
        int a = m / 5;
        for (int i = 0; i < 11; i++) {
            minute.append(a > i ? ((i+1) % 3 == 0 ? RED : YELLOW) : OFF);
        }
        minute.append(NEWLINE);

        // 输出第五排的亮灯情况
        // 分钟对5求余就是亮灯的数量，剩余的不亮
        int b = m % 5;
        for (int i = 0; i < 4; i++) {
            minute.append(b > i ? YELLOW : OFF);
        }

        return minute.toString();
    }

    private String convertSecond(int s) {
        // 输出第一排的亮灯情况
        // 秒是偶数黄灯亮， 奇数则不亮
        return s % 2 == 0 ? YELLOW : OFF;
    }
}
